﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Rewrite;
using Classes;

namespace Test_dotnet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            services.Configure<ForwardedHeadersOptions>(options =>
            {
                //options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto;
                options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.All;
                    //ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseForwardedHeaders();

/*
            app.Use(async (context, next) =>
                {
                    if (
                        //
                         context.Request.Headers["X-Forwarded-Proto"] == Uri.UriSchemeHttp)
                        //|| context.Request.Headers["http_x_forwarded_proto"] == "http")
                    {
                        string queryString = context.Request.QueryString.HasValue ? context.Request.QueryString.Value : string.Empty;
                        var https = "https://" + context.Request.Host + context.Request.Path + queryString;
                        context.Response.Redirect(https);
                    }
                    else
                    {
                        await next();
                    }
                });
*/
            //app.UseHttpsRedirect();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
            }

            //var options = new RewriteOptions().AddRedirectToHttpsPermanent();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

           // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
