

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Extensions.Caching.Memory;

namespace Test_dotnet.Controllers
{
    public class FloodDataResponse
    {
        public Classes.Marker[] Markers { get; set; }
        public int NumberGatesTotal { get; set; }     
        public int NumberGatesOpen { get; set; }     
        public int NumberGatesClosed { get; set; }     
        public string Message { get; set; }     
        public DateTimeOffset DateLastQuery { get; set; }    
    }

    [Route("api/[controller]")]
    [ApiController]
    public class AtxFloodsController : ControllerBase
    {
        private IMemoryCache _cache;

        public AtxFloodsController(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            try
            {
                var cache = _cache.GetOrCreate("ATX_FLOODS", entry => {
                    entry.SlidingExpiration = TimeSpan.FromSeconds(60);

                    var link = "https://www.atxfloods.com/dashboard/phpsqlajax_genxml.php";
                    var net = new System.Net.WebClient();
                    var data = net.DownloadData(link);
                    var content = new System.IO.MemoryStream(data);
                    var sr = new System.IO.StreamReader(content);

                    XmlSerializer serializer = new XmlSerializer(typeof(Classes.markers));
                    Classes.markers atxFloodsResponse = (Classes.markers)serializer.Deserialize(sr);
                    
                    var resp = new FloodDataResponse{
                        Markers = atxFloodsResponse.Marker,
                        NumberGatesTotal = atxFloodsResponse.Marker.Count(),
                        NumberGatesOpen = atxFloodsResponse.Marker.Count(m => m.Type == "on"),
                        NumberGatesClosed = atxFloodsResponse.Marker.Count(m => m.Type == "off"),
                        DateLastQuery = DateTimeOffset.Now
                    };

                    return resp;
                    
                });


                return Ok(cache);                
            }
            catch (System.Exception)
            {
                var resp = new FloodDataResponse{
                    Message = "No Data"
                };

                return Ok(resp);
            }

        }
    }
}
