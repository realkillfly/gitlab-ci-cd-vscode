using System;
using System.Xml.Serialization;

namespace Classes
{

        [SerializableAttribute()]
        [XmlTypeAttribute(AnonymousType = true)]
        [XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class markers
        {
            [XmlElementAttribute("marker")]
            public Marker[] Marker { get; set; }
        }

        [SerializableAttribute()]
        [XmlTypeAttribute(AnonymousType = true)]
        public partial class Marker
        {

            [XmlAttributeAttribute("name")]
            public string Name { get; set; }


            [XmlAttributeAttribute("jurisdiction")]
            public string Jurisdiction { get; set; }


            [XmlAttributeAttribute("address")]
            public string Address { get; set; }


            [XmlAttributeAttribute("lat")]
            public decimal Lat { get; set; }


            [XmlAttributeAttribute("lng")]
            public decimal Lng { get; set; }


            [XmlAttributeAttribute("type")]
            public string Type { get; set; }


            [XmlAttributeAttribute("comment")]
            public string Comment { get; set; }

 
            [XmlAttributeAttribute("id")]
            public ushort Id { get; set; }
        }



}
          