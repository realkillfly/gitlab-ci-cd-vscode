using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using System;

namespace Classes
{
    public class HttpsRedirectMiddleware
    {
        private readonly RequestDelegate _next;

        public HttpsRedirectMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var forwardedFor = context.Request.Headers["HTTP_X_FORWARDED_PROTO"];
            var isHttp = string.Equals(forwardedFor, "http");

            if(isHttp)
            {
                //var withHttps = Uri.UriSchemeHttps + Uri.SchemeDelimiter + context.Request. .Uri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.Scheme, UriFormat.SafeUnescaped);
                context.Response.Redirect("https://" + context.Request.Host + context.Request.PathBase);
            }

            // Call the next delegate/middleware in the pipeline
            await _next(context);
        }
    }

    public static class RequestCultureMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpsRedirect(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpsRedirectMiddleware>();
        }
    }
}